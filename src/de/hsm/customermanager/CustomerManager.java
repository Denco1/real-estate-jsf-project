package de.hsm.customermanager;

import java.util.List;



import de.hsm.persistence.Customer;
import de.hsm.persistence.Persistent;
import de.hsm.persistence.QueryName;



public class CustomerManager {
	private final Persistent persistent ;

public CustomerManager() {
	
	persistent= Persistent.getInstance();
	
}

public void registerCustomer( Customer customer) {
	persistent.addCustomer(customer);
	
}
public List<Customer> searchCustomer(String username)
{
	return persistent.executeQuery(Customer.class, QueryName.searchName,username);
}
public Customer loginCustomer(String userName, String password) {
	return persistent.find(Customer.class, userName, password);
	
}
public Customer editCustomer(String userName)
{
	return persistent.findCustomer(Customer.class, userName);
}




public Boolean checkEmail(String email)
{
	return persistent.findEmail(email);
}



}
