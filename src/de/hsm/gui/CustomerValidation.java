package de.hsm.gui;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import de.hsm.customermanager.CustomerManager;
import de.hsm.persistence.Customer;

@FacesValidator(value=CustomerValidation.VALIDATOR_ID)
public class CustomerValidation {
	
	public static final String VALIDATOR_ID = "de.hsm.persistence.Customer";
    
    private CustomerManager customerManager = new CustomerManager(); 
    
    public void validate(FacesContext ctx, UIComponent component, Customer customer)
            throws ValidatorException {
        if (customerManager.loginCustomer(customer.getUserName(), customer.getPassword()) == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                         "Your username or password is wrong", 
                         "Your username or password is wrong");
            throw new ValidatorException(msg);
        }
    
    }
}
