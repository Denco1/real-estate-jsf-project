package de.hsm.gui;


import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


import de.hsm.customermanager.CustomerManager;
import de.hsm.persistence.Customer;
import de.hsm.persistence.LoginPage;


@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {


    private Customer customer = new Customer();
    private CustomerManager customerManager = new CustomerManager();
    
    public Customer getCustomer() {
        return customer;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    private LoginPage loginpage= new LoginPage();
    

    public LoginPage getLoginpage() {
		return loginpage;
	}

	public void setLoginpage(LoginPage loginpage) {
		this.loginpage = loginpage;
	}

	public String login() {
        if (customerManager.loginCustomer(customer.getUserName(), loginpage.getPassword()) == null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username or password is wrong", "Username or password is wrong");
            FacesContext.getCurrentInstance().addMessage("login", msg);
            
            return "index.xhtml";
        }
        customerManager.loginCustomer(customer.getUserName(), loginpage.getPassword());
        return "selectionpage.xhtml";
    }
    
    public String register() {
        
        
        
        return "newcustomer.xhtml";
    }
    
    public String submit() {
        
        if(!customerManager.checkEmail(customer.getEmail())){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email is already in use", "Email is already in use");
            FacesContext.getCurrentInstance().addMessage("error email", msg);
            
            return "newcustomer.xhtml";
            
        }
        customerManager.registerCustomer(customer);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        
        return "index.xhtml";
    }
    
    public String changepassword()
    {
        return "changepassword.xhtml";
    }
    
    public String change()
    {
    	
        
        if(loginpage.getPassword().equals(loginpage.getNewPassword()))
        {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "New password is same as old one", "New password is same as old one");
            FacesContext.getCurrentInstance().addMessage("error password", msg);
            return "changepassword.xhtml";
        }
        
        customer.setPassword(loginpage.getNewPassword());
        
        customerManager.registerCustomer(customer);
        return "index.xhtml";
    }
    
    public String land() {
    	return "landproperty.xhtml";
    	
    }
    public String home() {
    	return "homeproperty.xhtml";
    	
    }
    public String edit()
    {
    	FacesContext facesContext = FacesContext.getCurrentInstance();
        //HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        
        
        System.out.println("First name in customer"+customer.getFirstName());
       
    	 return "editprofile.xhtml";
    	 
    	 
    }
    public String save() {
    	
    	
    	return "selectionpage.xhtml";
    }

 public String buy() {
    	
    	
    	return "payment.xhtml";
    }

 public String pay() {
 	
 	
 	return "confirmation.xhtml";
 }
}
