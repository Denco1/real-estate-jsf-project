package de.hsm.persistence;

public class LoginPage {
	
	private String userName;
	
	
	private String password;
	
	
	private String newPassword;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public LoginPage(String userName, String password, String newPassword) {
		super();
		this.userName = userName;
		this.password = password;
		this.newPassword = newPassword;
	}
	public LoginPage() {
		// TODO Auto-generated constructor stub
	}
	

}
